package com.mgl.isi.repository.jdbc;


import com.mgl.isi.domain.Participant;
import com.mgl.isi.domain.Program;
import com.mgl.isi.repository.ProgramRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcBasedProgramRepository implements ProgramRepository {
    private final DataSource dataSource;

    public JdbcBasedProgramRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    public List<Program> getAll()  {
        //requête sql pour récupèrer les infos
        String query = "SELECT * FROM program";
        //mapper le résultat
        List<Program> programs = new ArrayList<>();

        try {
            Connection connection = dataSource.createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query) ;

            while (rs.next()) {
                Program program = new Program();
                program.setId(rs.getLong("id"));
                program.setName(rs.getString("name"));

                programs.add(program);
            }
            return programs;

        }
        catch (Exception ex){
            return new ArrayList<Program>();
        }

    }

    public Program getById(int id) {
        String query = "SELECT id, name from program where id = ?";
        try {
            Connection connection = dataSource.createConnection();
            PreparedStatement statement = connection.prepareStatement(query);

            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();

            Program program = new Program();
            //if(rs.next()){
            int idr = rs.getInt("id");
            program.setId(new Long(idr));
            String fn = rs.getString("name");
            program.setName(fn);
            //}
            return program;
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
}
