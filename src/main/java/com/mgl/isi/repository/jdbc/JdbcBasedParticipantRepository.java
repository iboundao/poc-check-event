package com.mgl.isi.repository.jdbc;

import com.mgl.isi.domain.Panelist;
import com.mgl.isi.domain.Participant;
import com.mgl.isi.repository.ParticipantRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class JdbcBasedParticipantRepository implements ParticipantRepository {
    private final DataSource dataSource;

    public JdbcBasedParticipantRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    public List<Participant> getAll()  {
        //requête sql pour récupèrer les infos
        String query = "SELECT * FROM participant";
        //mapper le résultat
        List<Participant> participants = new ArrayList<>();

        try {
            Connection connection = dataSource.createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query) ;

            while (rs.next()) {
                Participant participant = new Participant();
                participant.setId(rs.getLong("id"));
                participant.setFirstName(rs.getString("first_name"));
                participant.setLastName(rs.getString("last_name"));

                participants.add(participant);
            }
            return participants;

        }
        catch (Exception ex){
            return new ArrayList<Participant>();
        }

    }

    public Participant getById(int id) {
        String query = "SELECT id, first_name, last_name from participant where id = ?";
        try {
            Connection connection = dataSource.createConnection();
            PreparedStatement statement = connection.prepareStatement(query);

            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();

            Participant participant = new Participant();
            //if(rs.next()){
                int idr = rs.getInt("id");
                participant.setId(new Long(idr));
                String fn = rs.getString("first_name");
                participant.setFirstName(fn);
                String ln = rs.getString("last_name");
                participant.setLastName(ln);
            //}
            return participant;
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    public int save(Participant participant) {
        String query = "INSERT INTO participant(id, first_name, last_name, profession, email, phone_number, company, created_at, category_id) values (NULL ,?,?,?,?,?,?,?,NULL )";
        try {
            Connection connection = dataSource.createConnection();
            PreparedStatement statement = connection.prepareStatement(query);

            Calendar calendar = Calendar.getInstance();
            java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());

            statement.setString(1, participant.getFirstName());
            statement.setString(2, participant.getLastName());
            statement.setString(3, participant.getProfession());
            statement.setString(4, participant.getEmail());
            statement.setString(5, participant.getPhoneNumber());
            statement.setString(6, participant.getCompany());
            statement.setDate(7, startDate);
            statement.execute();

            return 1;
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return 0;
    }
}
