package com.mgl.isi.repository.jdbc;

import com.mgl.isi.domain.Panelist;
import com.mgl.isi.domain.Participant;
import com.mgl.isi.repository.PanelistRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class JdbcBasedPanelistRepository implements PanelistRepository {
    private final DataSource dataSource;

    public JdbcBasedPanelistRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    public List<Panelist> getAll()  {
        //requête sql pour récupèrer les infos
        String query = "SELECT * FROM panelist";
        //mapper le résultat
        List<Panelist> panelists = new ArrayList<>();

        try {
            Connection connection = dataSource.createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query) ;

            while (rs.next()) {
                Panelist panelist = new Panelist();
                panelist.setId(rs.getLong("id"));
                panelist.setName(rs.getString("name"));

                panelists.add(panelist);
            }
            return panelists;

        }
        catch (Exception ex){
            return new ArrayList<Panelist>();
        }

    }

    public Panelist getById(int id) {
        String query = "SELECT id, name from panelist where id = ?";
        try {
            Connection connection = dataSource.createConnection();
            PreparedStatement statement = connection.prepareStatement(query);

            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();

            Panelist panelist = new Panelist();
            //if(rs.next()){
            int idr = rs.getInt("id");
            panelist.setId(new Long(idr));
            String fn = rs.getString("name");
            panelist.setName(fn);
            //}
            return panelist;
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
}
