package com.mgl.isi.repository.jdbc;

import com.mgl.isi.domain.Participant;
import com.mgl.isi.domain.ParticipantProgram;
import com.mgl.isi.repository.ParticipantProgramRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class JdbcBasedParticipantProgramRepository implements ParticipantProgramRepository {
    private final DataSource dataSource;

    public JdbcBasedParticipantProgramRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    public List<ParticipantProgram> getAll()  {
        //requête sql pour récupèrer les infos
        String query = "SELECT * FROM participant_program";
        //mapper le résultat
        List<ParticipantProgram> participants = new ArrayList<>();

        try {
            Connection connection = dataSource.createConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query) ;

            while (rs.next()) {
                ParticipantProgram participant = new ParticipantProgram();
                participant.setId(rs.getLong("id"));
                participant.setParticipant(rs.getLong("participant_id"));
                participant.setProgram(rs.getLong("program_id"));

                participants.add(participant);
            }
            return participants;

        }
        catch (Exception ex){
            return new ArrayList<ParticipantProgram>();
        }

    }

    public int save(ParticipantProgram participant) {
        String query = "INSERT INTO participant_program(id, participant_id, program_id) values (NULL ,?,?)";
        try {
            Connection connection = dataSource.createConnection();
            PreparedStatement statement = connection.prepareStatement(query);

            statement.setLong(1, participant.getProgram());
            statement.setLong(2, participant.getParticipant());
            statement.execute();

            return 1;
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return 0;
    }
    public ParticipantProgram getById(int id) {
        String query = "SELECT * from participant_program where id = ?";
        try {
            Connection connection = dataSource.createConnection();
            PreparedStatement statement = connection.prepareStatement(query);

            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();

            ParticipantProgram participant = new ParticipantProgram();
            //if(rs.next()){
            int idr = rs.getInt("id");
            participant.setId(new Long(idr));
            int fn = rs.getInt("program_id");
            participant.setProgram(new Long(fn));
            int ln = rs.getInt("participant_id");
            participant.setParticipant(new Long(ln));
            //}
            return participant;
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
}
