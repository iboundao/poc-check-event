package com.mgl.isi.repository;


import com.mgl.isi.domain.Panelist;

import java.util.List;

public interface PanelistRepository {
    List<Panelist> getAll();
}
