package com.mgl.isi.repository;

import com.mgl.isi.domain.Participant;

import java.util.List;

public interface ParticipantRepository {
    List<Participant> getAll();
    Participant getById(int id);
    int save(Participant participant);
}
