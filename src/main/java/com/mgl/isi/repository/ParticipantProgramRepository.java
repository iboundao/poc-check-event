package com.mgl.isi.repository;

import com.mgl.isi.domain.ParticipantProgram;

import java.util.List;

public interface ParticipantProgramRepository {
    List<ParticipantProgram> getAll();
    int save(ParticipantProgram participant);
    ParticipantProgram getById(int id);
}
