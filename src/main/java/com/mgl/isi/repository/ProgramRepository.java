package com.mgl.isi.repository;

import com.mgl.isi.domain.Program;

import java.util.List;

public interface ProgramRepository{
    List<Program> getAll();
}
