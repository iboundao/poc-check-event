package com.mgl.isi.service;

import com.mgl.isi.domain.Panelist;

import java.util.List;

public interface PanelistService {
    List<Panelist> getAll();
    int save(Panelist panelist);
    void displayMenu();
}
