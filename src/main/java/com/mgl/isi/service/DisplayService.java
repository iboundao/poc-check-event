package com.mgl.isi.service;

public interface DisplayService {
    void displayWelcome();
    void displayProfilMenu();
    void displayAdminMenu();
    void displayHoteMenu();
    void displayParticipantMenu();
}
