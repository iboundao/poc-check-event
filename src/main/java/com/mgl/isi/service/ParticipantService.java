package com.mgl.isi.service;

import com.mgl.isi.domain.Participant;

import java.util.List;

public interface ParticipantService {
    List<Participant> getAll();
    Participant findById(int id);
    int save(Participant participant);
    void displayMenu();
    Participant inputParticipant();
}
