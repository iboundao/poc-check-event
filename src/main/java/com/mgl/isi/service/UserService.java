package com.mgl.isi.service;

import com.mgl.isi.domain.User;

import java.util.List;

public interface UserService {
    List<User> getAll();
    User getById(int id);
    void displayMenu();
}
