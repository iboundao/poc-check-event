package com.mgl.isi.service;

import com.mgl.isi.domain.ParticipantProgram;

import java.util.List;

public interface ParticipantProgramService {
    List<ParticipantProgram> getAll();
    int save(ParticipantProgram participantProgram);
}
