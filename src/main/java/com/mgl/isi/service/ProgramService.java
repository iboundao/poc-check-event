package com.mgl.isi.service;

import com.mgl.isi.domain.Program;

import java.util.List;

public interface ProgramService {
    List<Program> getAll();
    int save(Program program);
    void displayMenu();
}
