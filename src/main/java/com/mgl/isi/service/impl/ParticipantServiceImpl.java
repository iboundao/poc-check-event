package com.mgl.isi.service.impl;

import com.mgl.isi.domain.Participant;
import com.mgl.isi.repository.ParticipantRepository;
import com.mgl.isi.service.ParticipantService;

import java.util.List;
import java.util.Scanner;

public class ParticipantServiceImpl implements ParticipantService {
    private final ParticipantRepository participantRepository;

    public ParticipantServiceImpl(ParticipantRepository participantRepository) {
        this.participantRepository = participantRepository;
    }

    public List<Participant> getAll() {
        List<Participant> participants = participantRepository.getAll();
        return participants;
    }

    public Participant findById(int id) {
        Participant participant = participantRepository.getById(id);
        return participant;
    }

    public int save(Participant participant) {
        return participantRepository.save(participant);
    }

    public void displayMenu() {
        System.out.println("1 - Liste des participants");
        System.out.println("2 - Ajouter un participant");
    }

    public Participant inputParticipant(){
        Participant participant = new Participant();
        Scanner sc = new Scanner(System.in);

        System.out.println("Entrer le prénom");
        participant.setFirstName(sc.nextLine());
        System.out.println("Entrer le nom");
        participant.setLastName(sc.nextLine());
        System.out.println("Entrer la profession");
        participant.setProfession(sc.nextLine());
        System.out.println("Entrer la compagnie");
        participant.setCompany(sc.nextLine());
        System.out.println("Entrer l' email");
        participant.setEmail(sc.nextLine());
        System.out.println("Entrer le numero de téléphone");
        participant.setPhoneNumber(sc.nextLine());

        return participant;
    }
}
