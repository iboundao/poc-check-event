package com.mgl.isi.service.impl;

import com.mgl.isi.domain.Participant;
import com.mgl.isi.domain.ParticipantProgram;
import com.mgl.isi.repository.ParticipantProgramRepository;
import com.mgl.isi.service.ParticipantProgramService;

import java.util.List;
import java.util.Scanner;

public class ParticipantProgramServiceImpl implements ParticipantProgramService {
    private final ParticipantProgramRepository participantProgramRepository;

    public ParticipantProgramServiceImpl(ParticipantProgramRepository participantProgramRepository) {
        this.participantProgramRepository = participantProgramRepository;
    }

    public List<ParticipantProgram> getAll() {
        return participantProgramRepository.getAll();
    }

    public int save(ParticipantProgram participantProgram) {
        return participantProgramRepository.save(participantProgram);
    }

    public ParticipantProgram inputParticipant(){
        ParticipantProgram participant = new ParticipantProgram();
        Scanner sc = new Scanner(System.in);

        System.out.println("Entrer l'id du participant'");
        participant.setParticipant(sc.nextLong());
        System.out.println("Entrer l'id du programme");
        participant.setProgram(sc.nextLong());

        return participant;
    }
}
