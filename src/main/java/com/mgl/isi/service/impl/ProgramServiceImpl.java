package com.mgl.isi.service.impl;

import com.mgl.isi.domain.Program;
import com.mgl.isi.repository.ProgramRepository;
import com.mgl.isi.service.ProgramService;

import java.util.List;

public class ProgramServiceImpl implements ProgramService {

    private final ProgramRepository programRepository;

    public ProgramServiceImpl(ProgramRepository programRepository) {
        this.programRepository = programRepository;
    }

    public List<Program> getAll() {
        List<Program> programs = programRepository.getAll();
        return programs;
    }

    public int save(Program program) {
        return 0;
    }

    public void displayMenu() {
        System.out.println("1 - Liste des programmes");
        //System.out.println("2 - Ajouter un programme");
    }
}
