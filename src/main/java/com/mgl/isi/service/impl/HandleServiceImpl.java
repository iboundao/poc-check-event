package com.mgl.isi.service.impl;

import com.mgl.isi.domain.*;
import com.mgl.isi.repository.jdbc.*;
import com.mgl.isi.service.*;
import com.sun.xml.internal.ws.wsdl.writer.document.Part;

import java.util.List;
import java.util.Scanner;

public class HandleServiceImpl implements HandleService {

    private final DisplayService displayService;
    private final UserService userService;
    private final ProgramService programService;
    private final PanelistService panelistService;
    private final ParticipantService participantService;
    private final ParticipantProgramService participantProgramService;
    private final Scanner scanner;

    public HandleServiceImpl() {
        this.displayService = new DisplayServiceImpl();
        this.userService = new UserServiceImpl();
        DataSource dataSource = new MysqlDataSource();
        JdbcBasedProgramRepository jdbcBasedProgramRepository = new JdbcBasedProgramRepository(dataSource);
        this.programService = new ProgramServiceImpl(jdbcBasedProgramRepository);
        JdbcBasedPanelistRepository jdbcBasedPanelistRepository = new JdbcBasedPanelistRepository(dataSource);
        this.panelistService = new PanelistServiceImpl(jdbcBasedPanelistRepository);
        JdbcBasedParticipantRepository jdbcBasedParticipantRepository = new JdbcBasedParticipantRepository(dataSource);
        this.participantService = new ParticipantServiceImpl(jdbcBasedParticipantRepository);
        JdbcBasedParticipantProgramRepository jdbcBasedParticipantProgramRepository = new JdbcBasedParticipantProgramRepository(dataSource);
        this.participantProgramService = new ParticipantProgramServiceImpl(jdbcBasedParticipantProgramRepository);

        this.scanner = new Scanner(System.in);
    }

    private int choice() {
        return scanner.nextInt();
    }

    private void unknownChoice(){
        System.out.println("Choix inconnu");
    }

    public void handleChoice() {
        int choice = choice();
        switch (choice){
            case 1: //admin process
                displayService.displayAdminMenu();
                choice = choice();
                switch (choice){
                    case 1: //user menu
                        userService.displayMenu();
                        choice = choice();
                        switch (choice){
                            case 1:
                                List<User> users = userService.getAll();
                                for (int i = 0; i < users.size(); i++) {
                                    System.out.println("");
                                }
                                break;
                            case 2:
                                System.out.println("Entrez l'id");
                                choice= choice();
                                User user = userService.getById(choice);
                                if (user != null) {
                                    System.out.println("");
                                }
                                break;
                            default:
                                unknownChoice();
                        }
                        break;
                    case 2: // programme menu
                        programService.displayMenu();
                        choice = choice();
                        switch (choice){
                            case 1:
                                List<Program> programs = programService.getAll();
                                for (int i = 0; i < programs.size(); i++) {
                                    System.out.println(String.format("%s - %s", programs.get(i).getId(), programs.get(i).getName()));
                                }
                                break;
                            case 2:
                                //
                                break;
                            default:
                                unknownChoice();
                        }
                        break;
                    case 3: // paneliste menu
                        panelistService.displayMenu();
                        choice = choice();
                        switch (choice){
                            case 1:
                                List<Panelist> panelists = panelistService.getAll();
                                for (int i = 0; i < panelists.size(); i++) {
                                    System.out.println(String.format("%s - %s", panelists.get(i).getId(), panelists.get(i).getName()));
                                }
                                break;
                            case 2:
                                //
                                break;
                            default:
                                unknownChoice();
                        }
                        break;
                    case 4: // liste presence
                        List<ParticipantProgram> participants = participantProgramService.getAll();
                        for (int i = 0; i < participants.size(); i++) {
                            System.out.println(String.format("%s - %s %s", participants.get(i).getId(), participants.get(i).getId(), participants.get(i).getProgram()));
                        }
                        break;
                    default:
                        unknownChoice();
                }
                break;
            case 2: //hote process
                displayService.displayHoteMenu();
                choice = choice();
                switch (choice){
                    case 1: //program list
                        List<Program> programs = programService.getAll();
                        for (int i = 0; i < programs.size(); i++) {
                            System.out.println(String.format("%s - %s", programs.get(i).getId(), programs.get(i).getName()));
                        }
                        break;
                    case 2: // participant list
                        List<Participant> participants = participantService.getAll();
                        for (int i = 0; i < participants.size(); i++) {
                            System.out.println(String.format("%s - %s %s", participants.get(i).getId(), participants.get(i).getFirstName(), participants.get(i).getLastName()));
                        }
                        break;
                    case 3: // find participant
                        System.out.println("entrer l'id");
                        choice=choice();
                        Participant participant = participantService.findById(choice);
                        if (participant != null) {
                            System.out.println(String.format("%s - %s %s", participant.getId(), participant.getFirstName(), participant.getLastName()));
                        }else {
                            unknownChoice();
                        }

                        break;
                    case 4: // inscription
                        Participant participantInput = participantService.inputParticipant();
                        int result = participantService.save(participantInput);
                        if (result == 1) {
                            System.out.println("Enregistrement effectué avec succès");
                        }else {
                            System.out.println("Une erreur s'est produite");
                        }
                        break;
                    default:
                        unknownChoice();
                }
                break;
            case 3: //participant process
                displayService.displayParticipantMenu();
                choice = choice();
                switch (choice){
                    case 1: //program list
                        List<Program> programs = programService.getAll();
                        for (int i = 0; i < programs.size(); i++) {
                            System.out.println(String.format("%s - %s", programs.get(i).getId(), programs.get(i).getName()));
                        }
                        break;
                    case 2: // panelist list
                        List<Panelist> panelists = panelistService.getAll();
                        for (int i = 0; i < panelists.size(); i++) {
                            System.out.println(String.format("%s - %s", panelists.get(i).getId(), panelists.get(i).getName()));
                        }
                        break;
                    case 3: // inscription
                        //panelistService.displayMenu();
                        Participant participant = participantService.inputParticipant();
                        int result = participantService.save(participant);
                        if (result == 1) {
                            System.out.println("Enregistrement effectué avec succès");
                        }else {
                            System.out.println("Une erreur s'est produite");
                        }
                        break;
                    default:
                        unknownChoice();
                }
                break;
            default:
                unknownChoice();
        }
    }
}
