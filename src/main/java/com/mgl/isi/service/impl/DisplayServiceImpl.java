package com.mgl.isi.service.impl;

import com.mgl.isi.service.DisplayService;

public class DisplayServiceImpl implements DisplayService {
    @Override
    public void displayWelcome() {
        System.out.println("**********************************************");
        System.out.println("* Bienvenue sur votre plateforme Check-Event *");
        System.out.println("**********************************************");
    }

    @Override
    public void displayProfilMenu() {
        System.out.println("Choisir un profil :");
        System.out.println("1 - Administrateur");
        System.out.println("2 - Hote / Hotesse");
        System.out.println("3 - Participant");
    }

    @Override
    public void displayAdminMenu() {
        //System.out.println("1 - Gestion des utilisateurs");
        System.out.println("2 - Gestion des programmes");
        System.out.println("3 - Gestion des panelistes");
        System.out.println("4 - Liste des présences");
    }

    @Override
    public void displayHoteMenu() {
        System.out.println("1 - Liste des programmes");
        System.out.println("2 - Liste des participants");
        System.out.println("3 - Rechercher un participant");
        System.out.println("4 - Inscrire un participant");
    }

    @Override
    public void displayParticipantMenu() {
        System.out.println("1 - Liste des programmes");
        System.out.println("2 - Liste des panelistes");
        System.out.println("3 - S'inscrire");
    }
}
