package com.mgl.isi.service.impl;

import com.mgl.isi.domain.Panelist;
import com.mgl.isi.domain.Program;
import com.mgl.isi.repository.PanelistRepository;
import com.mgl.isi.repository.ProgramRepository;
import com.mgl.isi.service.PanelistService;

import java.util.List;

public class PanelistServiceImpl implements PanelistService {

    private final PanelistRepository panelistRepository;

    public PanelistServiceImpl(PanelistRepository panelistRepository) {
        this.panelistRepository = panelistRepository;
    }

    public List<Panelist> getAll() {
        List<Panelist> panelists = panelistRepository.getAll();
        return panelists;
    }

    public int save(Panelist panelist) {
        return 0;
    }

    public void displayMenu() {
        System.out.println("1 - Liste des panélistes");
        //System.out.println("2 - Ajouter un panélistes");
    }
}
