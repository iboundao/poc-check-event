package com.mgl.isi.domain;


public class ParticipantProgram {

    private Long id;

    private Long participant;

    private Long program;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParticipant() {
        return participant;
    }

    public void setParticipant(Long participant) {
        this.participant = participant;
    }

    public Long getProgram() {
        return program;
    }

    public void setProgram(Long program) {
        this.program = program;
    }

    @Override
    public String toString() {
        return "ParticipantProgram{" +
                "id=" + id +
                ", participant=" + participant +
                ", program=" + program +
                '}';
    }
}
