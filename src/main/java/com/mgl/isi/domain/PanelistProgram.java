package com.mgl.isi.domain;


import java.time.Instant;

public class PanelistProgram {


    private Long id;

    private Instant date;

    private Panelist panelist;

    private Program program;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Panelist getPanelist() {
        return panelist;
    }

    public void setPanelist(Panelist panelist) {
        this.panelist = panelist;
    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    @Override
    public String toString() {
        return "PanelistProgram{" +
                "id=" + id +
                ", date=" + date +
                ", panelist=" + panelist +
                ", program=" + program +
                '}';
    }
}
