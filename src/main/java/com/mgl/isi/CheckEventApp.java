package com.mgl.isi;

import com.mgl.isi.controller.MainController;
import com.mgl.isi.service.impl.DisplayServiceImpl;
import com.mgl.isi.service.impl.HandleServiceImpl;

public class CheckEventApp {
    public static void main(String[] args) {
        MainController mainController = new MainController();
        mainController.process();
    }
}
