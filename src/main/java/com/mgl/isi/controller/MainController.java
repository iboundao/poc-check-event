package com.mgl.isi.controller;

import com.mgl.isi.service.DisplayService;
import com.mgl.isi.service.HandleService;
import com.mgl.isi.service.impl.DisplayServiceImpl;
import com.mgl.isi.service.impl.HandleServiceImpl;

public class MainController {

    public final DisplayService displayService;
    public final HandleService handleService;

    public MainController() {
        this.displayService = new DisplayServiceImpl();
        this.handleService = new HandleServiceImpl();
    }

    public void process(){
        displayService.displayWelcome();
        displayService.displayProfilMenu();
        handleService.handleChoice();
    }


    /*public void main(String[] args) {

        int choice = 100;
        Scanner scanner = new Scanner(System.in);

        while (choice!=0){
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<");
            System.out.println("Bienvenue dans votre gestionnaire d'événements!");
            System.out.println("Choisir une option: ");
            System.out.println("1 - Liste de présence");
            System.out.println("2 - Liste des inscrits");
            System.out.println("3 - Liste des programmes");
            System.out.println("4 - Liste des animateurs");
            System.out.println("0 - Quitter");
            choice=scanner.nextInt();
            switch (choice){
                case 1:
                    System.out.println(">>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<");
                    System.out.println("Liste des présences");
                    System.out.println("1- El Hadji Ibrahima NDAO");
                    System.out.println("2- Awa SALL");
                    System.out.println("3- Bassirou BITEYE");
                    System.out.println("4- Nancy SALL");
                    System.out.println("5- Assane SOW");
                    System.out.println("6- Fanta KONATE");
                    System.out.println("7- Abdoulaye NDOYE");
                    System.out.println("8- Ibrahima GUISSE");
                    System.out.println("9- Rama BA");
                    break;
                case 2:
                    System.out.println(">>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<");
                    System.out.println("Liste des Inscrits");
                    System.out.println("1- El Hadji Ibrahima NDAO");
                    System.out.println("2- Awa SALL");
                    System.out.println("3- Bassirou BITEYE");
                    System.out.println("4- Nancy SALL");
                    System.out.println("5- Assane SOW");
                    System.out.println("6- Fanta KONATE");
                    System.out.println("7- Abdoulaye NDOYE");
                    System.out.println("8- Ibrahima GUISSE");
                    System.out.println("9- Rama BA");
                    System.out.println("10- Mareme BA");
                    System.out.println("11- Moussa FALL");
                    break;
                case 3:
                    System.out.println(">>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<");
                    System.out.println("Liste des programmes");
                    System.out.println("1- Panel sur l'intelligence artificielle");
                    System.out.println("2- Atelier sur la robotique");
                    System.out.println("3- Conférence sur l'entreprenariat");
                    break;
                case 4:
                    System.out.println(">>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<");
                    System.out.println("Liste des animateurs");
                    System.out.println("1- Ndongo SAMB TONUX");
                    System.out.println("2- Elias BA");
                    System.out.println("3- Baye NIASS");
                    System.out.println("4- Serigne BARRO");
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Option unknown");
            }
        }
    }*/
}
