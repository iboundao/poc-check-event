-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 15, 2021 at 08:04 PM
-- Server version: 5.7.32
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `poc`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `theme` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `name`, `theme`, `location`, `start_date`, `end_date`) VALUES
(1, 'Tasty Plastic Tuna cross-platform connect', 'Concrete portals', 'CFA Franc BEAC bluetooth Missouri', '2021-03-11 21:00:49', '2021-03-12 02:21:41'),
(2, 'SDD', 'back-end generating', 'array hack Shoes', '2021-03-12 10:36:25', '2021-03-12 06:28:19'),
(3, 'architectures 24/7', 'data-warehouse Home Loan Account', 'copying', '2021-03-12 13:28:22', '2021-03-12 00:46:07'),
(4, 'Paradigm Triple-buffered', 'Car connecting radical', 'transmitting', '2021-03-12 05:49:58', '2021-03-12 11:09:15'),
(5, 'hack Administrator maximize', 'Handmade Metal Towels', 'Legacy Auto Loan Account', '2021-03-12 10:47:56', '2021-03-12 10:34:09'),
(6, 'Gorgeous well-modulated', 'Paradigm RSS withdrawal', 'withdrawal Bedfordshire', '2021-03-12 17:12:47', '2021-03-12 09:16:12'),
(7, 'Steel Metal', 'New Hampshire Bedfordshire', 'deposit', '2021-03-12 12:21:06', '2021-03-12 12:59:09'),
(8, 'Nauru Manat Gorgeous Fresh Computer', 'mint green Team-oriented', 'Handcrafted PCI', '2021-03-12 04:27:27', '2021-03-12 01:19:18'),
(9, 'Rustic', 'Cordoba Oro Table back up', 'modular virtual', '2021-03-12 09:44:20', '2021-03-12 08:29:40'),
(10, 'THX Buckinghamshire', 'Indiana', 'Steel Movies Avon', '2021-03-12 05:41:47', '2021-03-12 09:11:08');

-- --------------------------------------------------------

--
-- Table structure for table `panelist`
--

CREATE TABLE `panelist` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `panelist`
--

INSERT INTO `panelist` (`id`, `name`, `email`, `phone_number`) VALUES
(1, 'Salad generate Metal', 'Bo.Konopelski15@hotmail.com', 'Salad Virgin Islands, U.S.'),
(2, 'Nevada Generic Cotton Bike South Carolina', 'Crystel_Bradtke@hotmail.com', 'extend Ergonomic'),
(3, 'Officer', 'Demarco.Beatty@gmail.com', 'Berkshire Street'),
(4, 'Handmade Customer', 'Tevin.Gislason@hotmail.com', 'portals Alley'),
(5, 'Ireland violet', 'William_Cremin86@gmail.com', 'Steel azure Soft'),
(6, 'Industrial auxiliary', 'Opal50@gmail.com', 'Manager'),
(7, 'Future-proofed optical', 'Chloe_Ruecker@hotmail.com', 'interactive Avon Assimilated'),
(8, 'Grass-roots backing up', 'Eda_Balistreri70@yahoo.com', 'Specialist'),
(9, 'Grass-roots compress Computers', 'Hellen_Medhurst25@hotmail.com', 'robust Credit Card Account parse'),
(10, '1080p Investor Falls', 'Emiliano.Monahan59@hotmail.com', 'Credit Card Account');

-- --------------------------------------------------------

--
-- Table structure for table `panelist_program`
--

CREATE TABLE `panelist_program` (
  `id` bigint(20) NOT NULL,
  `date` datetime DEFAULT NULL,
  `panelist_id` bigint(20) DEFAULT NULL,
  `program_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `panelist_program`
--

INSERT INTO `panelist_program` (`id`, `date`, `panelist_id`, `program_id`) VALUES
(1, '2021-03-11 18:03:32', NULL, NULL),
(2, '2021-03-12 01:06:30', NULL, NULL),
(3, '2021-03-12 04:58:53', NULL, NULL),
(4, '2021-03-12 11:13:21', NULL, NULL),
(5, '2021-03-11 17:50:30', NULL, NULL),
(6, '2021-03-12 00:29:58', NULL, NULL),
(7, '2021-03-11 19:05:17', NULL, NULL),
(8, '2021-03-12 03:17:26', NULL, NULL),
(9, '2021-03-11 18:29:35', NULL, NULL),
(10, '2021-03-11 22:47:18', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `participant`
--

CREATE TABLE `participant` (
  `id` bigint(20) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `profession` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `participant`
--

INSERT INTO `participant` (`id`, `first_name`, `last_name`, `profession`, `email`, `phone_number`, `company`, `created_at`, `category_id`) VALUES
(1, 'Joaquin', 'Marks', 'French Southern Territories invoice Tajikistan', 'Brittany_Metz47@hotmail.com', 'Human Ergonomic', 'multi-tasking', '2021-03-12 02:22:13', NULL),
(2, 'Brook', 'Towne', 'forecast HDD interface', 'Monserrat.Cole91@gmail.com', 'Run Avon overriding', 'Open-architected', '2021-03-12 15:25:37', NULL),
(3, 'Casimer', 'Kris', 'National mobile', 'Gerald50@hotmail.com', 'Awesome TCP Lakes', 'Kids withdrawal', '2021-03-12 08:15:30', NULL),
(4, 'Macy', 'Krajcik', 'Buckinghamshire', 'Jayme.Miller68@gmail.com', 'primary', 'hack', '2021-03-11 20:36:03', NULL),
(5, 'Yadira', 'Vandervort', 'XSS', 'Daniela.Spencer46@hotmail.com', 'parallelism Credit Card Account Soft', 'Strategist auxiliary', '2021-03-11 18:46:53', NULL),
(6, 'Fritz', 'Fisher', 'primary Bhutan benchmark', 'Lera.Hessel67@yahoo.com', 'Intranet Facilitator Grove', 'challenge Light Tasty', '2021-03-11 21:42:16', NULL),
(7, 'Oma', 'Turner', 'Function-based utilisation internet solution', 'Una99@hotmail.com', 'Practical Fresh Cheese integrated', 'bypassing Serbian Dinar', '2021-03-11 22:35:18', NULL),
(8, 'Lyla', 'Kautzer', 'deposit', 'London41@yahoo.com', 'synthesize Chicken Borders', 'partnerships moderator', '2021-03-12 09:01:27', NULL),
(9, 'Hardy', 'Schmitt', 'Ergonomic Frozen Computer Buckinghamshire index', 'Clementina_Kuphal@yahoo.com', 'Developer CSS', 'Borders Borders', '2021-03-11 22:14:51', NULL),
(10, 'Gwendolyn', 'Huel', 'monitor Tunisia infomediaries', 'Daphne.Huels65@yahoo.com', 'Arizona e-services Hawaii', 'Utah synergistic Fresh', '2021-03-12 08:13:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `participant_category`
--

CREATE TABLE `participant_category` (
  `id` bigint(20) NOT NULL,
  `category` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `participant_category`
--

INSERT INTO `participant_category` (`id`, `category`) VALUES
(1, 'transmitting 1080p'),
(2, 'compressing Unbranded Metal Bacon'),
(3, 'Lead'),
(4, 'Crescent Chair'),
(5, 'Intranet best-of-breed Islands'),
(6, 'Metal functionalities user-centric'),
(7, 'backing up'),
(8, 'Tuna payment'),
(9, 'XML South Carolina function'),
(10, 'Wooden');

-- --------------------------------------------------------

--
-- Table structure for table `participant_program`
--

CREATE TABLE `participant_program` (
  `id` bigint(20) NOT NULL,
  `participant_id` bigint(20) DEFAULT NULL,
  `program_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `participant_program`
--

INSERT INTO `participant_program` (`id`, `participant_id`, `program_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 2, 2),
(4, 3, 1),
(5, 1, 2),
(6, 4, 3),
(7, 4, 2),
(8, 5, 1),
(9, 6, 1),
(10, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  `event_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id`, `name`, `start_date`, `end_date`, `type_id`, `event_id`) VALUES
(1, 'Tugrik bypassing', '2021-03-12 15:43:29', '2021-03-12 14:28:39', NULL, NULL),
(2, 'District Principal haptic', '2021-03-12 09:44:27', '2021-03-11 17:45:25', NULL, NULL),
(3, 'pixel content-based white', '2021-03-12 07:27:33', '2021-03-12 16:29:09', NULL, NULL),
(4, 'Sleek Cotton Pants Cambridgeshire ubiquitous', '2021-03-12 00:26:37', '2021-03-12 13:11:21', NULL, NULL),
(5, 'Checking Account distributed', '2021-03-12 16:38:19', '2021-03-12 03:13:01', NULL, NULL),
(6, 'Sharable analyzer', '2021-03-12 03:06:31', '2021-03-12 13:50:16', NULL, NULL),
(7, 'SSL', '2021-03-11 18:29:07', '2021-03-12 05:10:05', NULL, NULL),
(8, 'French Southern Territories', '2021-03-12 14:23:41', '2021-03-11 19:35:58', NULL, NULL),
(9, 'success', '2021-03-12 17:23:14', '2021-03-11 23:40:49', NULL, NULL),
(10, 'Brunei Dollar Oklahoma', '2021-03-12 08:10:53', '2021-03-12 10:34:28', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `program_type`
--

CREATE TABLE `program_type` (
  `id` bigint(20) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `program_type`
--

INSERT INTO `program_type` (`id`, `type`) VALUES
(1, 'channels'),
(2, 'deposit'),
(3, 'wireless tangible Berkshire'),
(4, 'payment scalable'),
(5, 'Analyst calculating Loaf'),
(6, 'Cheese Handmade panel'),
(7, 'Tasty Metal Ball Steel'),
(8, 'green bandwidth'),
(9, 'Jordanian Dinar'),
(10, 'Regional protocol');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panelist`
--
ALTER TABLE `panelist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `panelist_program`
--
ALTER TABLE `panelist_program`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_panelist_program_panelist_id` (`panelist_id`),
  ADD KEY `fk_panelist_program_program_id` (`program_id`);

--
-- Indexes for table `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_participant_category_id` (`category_id`);

--
-- Indexes for table `participant_category`
--
ALTER TABLE `participant_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participant_program`
--
ALTER TABLE `participant_program`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_participant_program_participant_id` (`participant_id`),
  ADD KEY `fk_participant_program_program_id` (`program_id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_program_type_id` (`type_id`),
  ADD KEY `fk_program_event_id` (`event_id`);

--
-- Indexes for table `program_type`
--
ALTER TABLE `program_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `panelist`
--
ALTER TABLE `panelist`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `panelist_program`
--
ALTER TABLE `panelist_program`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `participant`
--
ALTER TABLE `participant`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `participant_category`
--
ALTER TABLE `participant_category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `participant_program`
--
ALTER TABLE `participant_program`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `program_type`
--
ALTER TABLE `program_type`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `panelist_program`
--
ALTER TABLE `panelist_program`
  ADD CONSTRAINT `fk_panelist_program_panelist_id` FOREIGN KEY (`panelist_id`) REFERENCES `panelist` (`id`),
  ADD CONSTRAINT `fk_panelist_program_program_id` FOREIGN KEY (`program_id`) REFERENCES `program` (`id`);

--
-- Constraints for table `participant`
--
ALTER TABLE `participant`
  ADD CONSTRAINT `fk_participant_category_id` FOREIGN KEY (`category_id`) REFERENCES `participant_category` (`id`);

--
-- Constraints for table `participant_program`
--
ALTER TABLE `participant_program`
  ADD CONSTRAINT `fk_participant_program_participant_id` FOREIGN KEY (`participant_id`) REFERENCES `participant` (`id`),
  ADD CONSTRAINT `fk_participant_program_program_id` FOREIGN KEY (`program_id`) REFERENCES `program` (`id`);

--
-- Constraints for table `program`
--
ALTER TABLE `program`
  ADD CONSTRAINT `fk_program_event_id` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  ADD CONSTRAINT `fk_program_type_id` FOREIGN KEY (`type_id`) REFERENCES `program_type` (`id`);
