package com.mgl.isi.repository.jdbc;

import com.mgl.isi.domain.Participant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JdbcBasedParticipantRepositoryTest {
    private JdbcBasedParticipantRepository jdbcBasedParticipantRepository;

    @BeforeEach
    void setUp() throws SQLException {
        System.out.println("Setup");
        //Arrange
        DataSource dataSource = new MockDatasource();
        jdbcBasedParticipantRepository = new JdbcBasedParticipantRepository(dataSource);
    }

    @Test
    void getByIdShouldReturnParticipantWhenAvailableNotTrivial() throws SQLException {
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);

        when(dataSource.createConnection()).thenReturn(connection);
        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        ResultSet resultSet2 = mock(ResultSet.class);
        when(preparedStatement.executeQuery()).thenReturn(resultSet2);
        when(resultSet2.getInt(anyString())).thenReturn(5);
        when(resultSet2.getString(anyString())).thenReturn("aaaaa");

        jdbcBasedParticipantRepository = new JdbcBasedParticipantRepository(dataSource);

        Participant participant = jdbcBasedParticipantRepository.getById(5);
        assertNotNull(participant);
        assertEquals(5, participant.getId());
        assertEquals("aaaaa", participant.getFirstName());
    }

    @Test
    void getAllShouldReturnParticipantWhenAvailableNotTrivial() throws SQLException {
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);

        when(dataSource.createConnection()).thenReturn(connection);
        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        ResultSet resultSet2 = mock(ResultSet.class);
        when(preparedStatement.executeQuery()).thenReturn(resultSet2);
        when(resultSet2.getInt(anyString())).thenReturn(5);
        when(resultSet2.getString(anyString())).thenReturn("aaaaa");

        jdbcBasedParticipantRepository = new JdbcBasedParticipantRepository(dataSource);

        List<Participant> participant = jdbcBasedParticipantRepository.getAll();
        assertNotNull(participant);
    }
}
