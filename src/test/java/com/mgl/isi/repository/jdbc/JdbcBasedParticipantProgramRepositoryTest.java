package com.mgl.isi.repository.jdbc;

import com.mgl.isi.domain.ParticipantProgram;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JdbcBasedParticipantProgramRepositoryTest {
    private JdbcBasedParticipantProgramRepository jdbcBasedParticipantProgramRepository;

    @BeforeEach
    void setUp() throws SQLException {
        System.out.println("Setup");
        //Arrange
        DataSource dataSource = new MockDatasource();
        jdbcBasedParticipantProgramRepository = new JdbcBasedParticipantProgramRepository(dataSource);
    }

    @Test
    void getByIdShouldReturnParticipantProgramWhenAvailableNotTrivial() throws SQLException {
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);

        when(dataSource.createConnection()).thenReturn(connection);
        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        ResultSet resultSet2 = mock(ResultSet.class);
        when(preparedStatement.executeQuery()).thenReturn(resultSet2);
        when(resultSet2.getInt(anyString())).thenReturn(5);
        when(resultSet2.getString(anyString())).thenReturn("aaaaa");

        jdbcBasedParticipantProgramRepository = new JdbcBasedParticipantProgramRepository(dataSource);

        ParticipantProgram ParticipantProgram = jdbcBasedParticipantProgramRepository.getById(5);
        assertNotNull(ParticipantProgram);
        assertEquals(5, ParticipantProgram.getId());
        assertEquals(5, ParticipantProgram.getProgram());
    }

    @Test
    void getAllShouldReturnParticipantProgramsWhenAvailableNotTrivial() throws SQLException {
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);

        when(dataSource.createConnection()).thenReturn(connection);
        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        ResultSet resultSet2 = mock(ResultSet.class);
        when(preparedStatement.executeQuery()).thenReturn(resultSet2);
        when(resultSet2.getInt(anyString())).thenReturn(5);
        when(resultSet2.getString(anyString())).thenReturn("aaaaa");

        jdbcBasedParticipantProgramRepository = new JdbcBasedParticipantProgramRepository(dataSource);

        List<ParticipantProgram> ParticipantProgram = jdbcBasedParticipantProgramRepository.getAll();
        assertNotNull(ParticipantProgram);
    }

}
