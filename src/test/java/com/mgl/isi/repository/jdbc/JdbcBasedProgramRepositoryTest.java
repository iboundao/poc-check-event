package com.mgl.isi.repository.jdbc;

import com.mgl.isi.domain.Program;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JdbcBasedProgramRepositoryTest {
    private JdbcBasedProgramRepository jdbcBasedProgramRepository;

    @BeforeEach
    void setUp() throws SQLException {
        System.out.println("Setup");
        //Arrange
        DataSource dataSource = new MockDatasource();
        jdbcBasedProgramRepository = new JdbcBasedProgramRepository(dataSource);
    }

    @Test
    void getByIdShouldReturnProgramWhenAvailableNotTrivial() throws SQLException {
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);

        when(dataSource.createConnection()).thenReturn(connection);
        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        ResultSet resultSet2 = mock(ResultSet.class);
        when(preparedStatement.executeQuery()).thenReturn(resultSet2);
        when(resultSet2.getInt(anyString())).thenReturn(5);
        when(resultSet2.getString(anyString())).thenReturn("aaaaa");

        jdbcBasedProgramRepository = new JdbcBasedProgramRepository(dataSource);

        Program Program = jdbcBasedProgramRepository.getById(5);
        assertNotNull(Program);
        assertEquals(5, Program.getId());
        assertEquals("aaaaa", Program.getName());
    }

    @Test
    void getAllShouldReturnProgramsWhenAvailableNotTrivial() throws SQLException {
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);

        when(dataSource.createConnection()).thenReturn(connection);
        Statement statement = mock(Statement.class);
        when(connection.createStatement()).thenReturn(statement);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        ResultSet resultSet2 = mock(ResultSet.class);
        when(preparedStatement.executeQuery()).thenReturn(resultSet2);
        when(resultSet2.getInt(anyString())).thenReturn(5);
        when(resultSet2.getString(anyString())).thenReturn("aaaaa");

        jdbcBasedProgramRepository = new JdbcBasedProgramRepository(dataSource);

        List<Program> Program = jdbcBasedProgramRepository.getAll();
        assertNotNull(Program);
    }

}
