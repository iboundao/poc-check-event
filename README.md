Description: Ce projet est destiné à la gestion d'un événement dans le cadre des salons numériques (ex: SIPEN). 
Il servira à gérer la préparation de l'événement, à savoir gérer les programmes, les panélistes et les inscritptions avant l'événement, et la gestion des émargements le jour de l'événement. 
Il permettra d'avoir une vue d'ensemble sur le taux de participants par rapport au nombre d'inscrits.

Profils: 
- Administrateur 
- Hôte/Hôtesse 
- Participant

Liste des fonctionnalistés: 
- Administrateur: 
    * Gestion des programmes (ajout, modification, recherche, etc) 
    * Gestion des panélistes (ajout, modification, suppression, recherche, etc) et affecter un panéliste à un programme 
    * Gestion des utilisateurs 
    * Accéder au dashboard 
- Hôte/Hôtesse: 
    * Consulter la liste des programmes 
    * Consulter la liste des participants 
    * Rechercher un participant 
    * Ajouter un participant à la liste de présence (emargement) 
    * Inscrire un participant 
- Participant 
    * S'inscrire 
    * Consulter les programmes 
    * Consulter la liste des panélistes